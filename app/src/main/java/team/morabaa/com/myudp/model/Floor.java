package team.morabaa.com.myudp.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

/**
 * Created by eagle on 11/25/2017.
 */
@Entity
public class Floor {
    @PrimaryKey
    private long id;
    String name;
    String floorId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloorId() {
        return floorId;
    }

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }

   
    @Dao
    public interface FloorDuo {
        
        @Query("SELECT * FROM Floor")
        List<Floor> FLOORS();
        
        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(Floor... floors);
        
        
        @Delete
        void delete(Floor... floors);
        
        @Update
        void Update(Floor... floors);
        @Query("SELECT COUNT(id) FROM Floor")
        int count();
    }
}
