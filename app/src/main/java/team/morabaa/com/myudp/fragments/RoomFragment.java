package team.morabaa.com.myudp.fragments;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static java.lang.System.out;
import static team.morabaa.com.myudp.activities.MainActivity.db;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import team.morabaa.com.myudp.R;
import team.morabaa.com.myudp.activities.MainActivity;
import team.morabaa.com.myudp.adapters.RoomAdapter;
import team.morabaa.com.myudp.interfaces.ActivityFragmentBridge;
import team.morabaa.com.myudp.interfaces.AdapterFragmentBridge;
import team.morabaa.com.myudp.model.Device;
import team.morabaa.com.myudp.model.Room;
import team.morabaa.com.myudp.model.TcpMessage;
import team.morabaa.com.myudp.tcp.TcpClient;
import team.morabaa.com.myudp.utils.SV;
import team.morabaa.com.myudp.utils.SV.TCP_COMMANDS;


public class RoomFragment extends Fragment
      implements AdapterFragmentBridge, ActivityFragmentBridge {
      
      public static TextView txtTemperature;
      public static ImageView imgDoor;
      public static ImageView imgMotion;
      public static TextView txtHumidity;
      public static Handler deviceHandler;
      public FragmentManager fragmentManager;
      String floor;
      long room;
      RecyclerView recyclerView;
      GridLayout gridDevices;
      List<Device> devices;
      ColorMatrixColorFilter filter;
      String page;
      String ARG_PAGE;
      LinearLayout addDevice;
      ProgressBar roomsWait;
      RoomAdapter roomAdapter;
      Button turnAllOff;
      Button turnAllOn;
      private Vibrator vibrator;
      
      @SuppressLint("HandlerLeak")
      public RoomFragment() {
            deviceHandler = deviceRouter();
      }
      
      public static ArrayList<View> getAllChildren(View v) {
            
            if (!(v instanceof ViewGroup)) {
                  ArrayList<View> viewArrayList = new ArrayList<>();
                  viewArrayList.add(v);
                  return viewArrayList;
            }
            
            ArrayList<View> result = new ArrayList<View>();
            
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                  
                  View child = vg.getChildAt(i);
                  
                  ArrayList<View> viewArrayList = new ArrayList<View>();
                  viewArrayList.add(v);
                  viewArrayList.addAll(getAllChildren(child));
                  
                  result.addAll(viewArrayList);
            }
            return result;
      }
      
      public void getReceivedDevice(Device device) {
            switch ((int) device.getDeviceId()) {
                  case 30: {
                        if (txtTemperature != null) {
                              txtTemperature
                                    .setText(device.getStatus() + "");
                        }
                        break;
                  }
                  case 33: {
                        if (txtHumidity != null) {
                              txtHumidity
                                    .setText(device.getStatus() + "");
                        }
                        break;
                  }
                  case 31: {
                        if (imgDoor != null) {
                              if (device.getStatus() == 0) {
                                    imgDoor.setImageResource(
                                          R.drawable.door_closed);
                              } else {
                                    imgDoor.setImageResource(
                                          R.drawable.door_opened);
                              }
                        }
                        break;
                  }
                  case 32: {
                        if (imgMotion != null) {
                              if (device.getStatus() == 0) {
                                    imgMotion.setImageResource(
                                          R.drawable.movement_true);
                              } else {
                                    imgMotion.setImageResource(
                                          R.drawable.movement_false);
                              }
                        }
                        break;
                  }
                  default: {
                        device.setStatus(
                              device.getStatus());
                        roomAdapter.updateDevice(device);
                        System.out.println("time received");

//                      roomAdapter.setDevices(devices);
                        break;
                  }
            }
      }
      
      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            floor = getArguments().getString("floor");
            room = getArguments().getLong("room");
      }
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            if (savedInstanceState != null && savedInstanceState.containsKey(ARG_PAGE)) {
                  //            if () {
                  page = savedInstanceState.getString(ARG_PAGE);
                  //            }
            }
            
            float[] colorMatrix = {1.0f, 1.0f, 1.0f, 0, 10, 1.0f, 1.0f, 1.0f, 0, 10, 1.0f, 1.0f,
                  1.0f, 0, 10, 0, 0, 0, 1, 0};
            filter = new ColorMatrixColorFilter(colorMatrix);
            
            return inflater.inflate(R.layout.fragment_third, container, false);
      }
      
      private Handler deviceRouter() {
            return new Handler(Looper.getMainLooper()) {
                  @SuppressLint("SetTextI18n")
                  public void handleMessage(Message msg) {
                        assert msg != null;
                        String message = msg.getData().getString("result");
                        final Device device = new Gson()
                              .fromJson(message, Device.class);
                        getReceivedDevice(device);
                  }
            };
      }
      
      @Override
      public void onViewCreated(View view, Bundle savedInstanceState) {
            ((MainActivity) getActivity()).setActivityFragmentBridge(this);
            
            turnAllOn = view.findViewById(R.id.turnAllOn);
            turnAllOff = view.findViewById(R.id.turnAllOff);
            imgDoor = view.findViewById(R.id.imgDoor);
            imgMotion = view.findViewById(R.id.imgMotion);
            txtTemperature = view.findViewById(R.id.txtTemperature);
            txtHumidity = view.findViewById(R.id.txtHumidity);
            
            txtTemperature.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        
                        Device device = new Device();
                        device.setControllerId(room);
                        device.setDeviceId(30);
                        device.setStatus(0);
                        final String message = new Gson().toJson(
                              new TcpMessage() {{
                                    setCommand(TCP_COMMANDS.GET);
                                    setDevice(device);
                              }}, TcpMessage.class
                        );
                        TcpClient.send(getContext(), message);
                  }
            });
            addDevice = view.findViewById(R.id.addDevice);
            addDevice.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        FragmentTransaction fragmentTransaction = getActivity()
                              .getSupportFragmentManager().beginTransaction();
                        SettingsFragment settingsFragment = new SettingsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("floor", floor);
                        bundle.putLong("roomId", room);
                        settingsFragment.setArguments(bundle);
                        MainActivity.currentFragment = settingsFragment;
                        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                              .replace(R.id.main, settingsFragment)
                              .addToBackStack(null)
                              .commit();
                  }
            });
            recyclerView = view.findViewById(R.id.grid);
            gridDevices = view.findViewById(R.id.gridDevices);
            roomsWait = view.findViewById(R.id.roomsWait);
            devices = new ArrayList<>();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            fragmentManager = getActivity().getSupportFragmentManager();
            vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
            
            roomAdapter = new RoomAdapter(
                  getContext(), devices, room, getActivity(), displayMetrics, this);
            roomAdapter.setAdapterFragmentBridge(this);
            recyclerView.setAdapter(roomAdapter);
//            ((MainActivity) getActivity()).setActivityFragmentBridge(new ActivityFragmentBridge() {
//                  @Override
//                  public void onDeviceReceived(Device device) {
//                        onBundleSelected(device);
//                  }
//            });
            turnAllOff.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        vibrator.vibrate(70);
                        for (Device d : devices) {
                              Device device = new Device();
                              device.setStatus(1);
                              device.setDeviceId(d.getDeviceId());
                              device.setControllerId(
                                    d.getControllerId());
                              device.setStatus(0);
                              final String message = new Gson().toJson(
                                    new TcpMessage() {{
                                          setCommand(TCP_COMMANDS.SET);
                                          setDevice(device);
                                    }}, TcpMessage.class
                              );
                              if (d.getStatus() > 0) {
                                    TcpClient.send(getContext(), message);
                              }
                        }
                  }
            });
            turnAllOn.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        vibrator.vibrate(70);
                        for (int i = devices.size() - 1; i >= 0; i--) {
                              Device device = new Device();
                              device.setStatus(1);
                              device.setDeviceId(devices.get(i).getDeviceId());
                              device.setControllerId(
                                    devices.get(i).getControllerId());
                              final String message = new Gson().toJson(
                                    new TcpMessage() {{
                                          setCommand(TCP_COMMANDS.SET);
                                          setDevice(device);
                                    }}, TcpMessage.class
                              );
                              if (devices.get(i).getStatus() < 1) {
                                    TcpClient.send(getContext(), message);
                              }
                        }
                  }
            });
            String ro = "";
            for (Room r : db.roomDuo().ROOMS()) {
                  if (r.getControllerId() == room) {
                        ro = r.getName();
                  }
            }
            getActivity().setTitle(ro);
            for (View view1 : getAllChildren(view)) {
                  handleBackPress(view1);
            }
      }
      
      @Override
      public void onResume() {
            super.onResume();
            reloadDevices();
            if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
                  recyclerView.setLayoutManager(
                        new GridLayoutManager(getContext(), 2)
                  );
            } else {
                  recyclerView.setLayoutManager(
                        new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, true)
                  );
            }
      }
      
      void reloadDevices() {
            devices = new ArrayList<>();
            try {
                  for (Device device : db.deviceDuo().DEVICES()) {
                        if (device.getRoomId() == room && (device.getType().equals("single")
                              || device.getType().equals("multiValue"))) {
                              devices.add(device);
                        }
                  }
            } catch (Exception ignored) {
            }
            Device device = new Device();
            device.setControllerId(room);
            device.setDeviceId(0);
            device.setStatus(0);
            final String message = new Gson().toJson(
                  new TcpMessage() {{
                        setCommand(TCP_COMMANDS.GET);
                        setDevice(device);
                  }}, TcpMessage.class
            );
            TcpClient.send(getContext(), message);
            roomAdapter.setDevices(devices);
            
      }
      
      @Override
      public void onPause() {
            super.onPause();
      }
      
      @Override
      public void onSaveInstanceState(Bundle outState) {
            outState.putString(ARG_PAGE, page);
            super.onSaveInstanceState(outState);
      }
      
      private void handleBackPress(View view) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(
                  new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                              if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    FragmentTransaction fragmentTransaction
                                          = getActivity().getSupportFragmentManager()
                                          .beginTransaction();
                                    HomeFragment homeFragment = new HomeFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("floor", "01");
                                    homeFragment.setArguments(bundle);
                                    fragmentTransaction.replace(R.id.main, homeFragment)
                                          .addToBackStack(null).commit();
                                    out.println("back pressed ^_^");
                                    return true;
                              }
                              return false;
                        }
                  }
            );
      }
      
      @Override
      public void onDeviceClicked(int position) {
            vibrator.vibrate(30);
            if (devices.get(position).getType().equals("single")) {
                  int s;
                  if (devices.get(position).getStatus() == 0) {
                        s = 1;
                  } else {
                        s = 0;
                  }
                  Device device = new Device();
                  device.setControllerId(room);
                  device.setDeviceId(devices.get(position).getDeviceId());
                  device.setStatus(s);
                  final String message = new Gson().toJson(
                        new TcpMessage() {{
                              setCommand(TCP_COMMANDS.SET);
                              setDevice(device);
                        }}, TcpMessage.class
                  );
                  TcpClient.send(getContext(), message);
                  System.out.println("time send");
                  
            } else {
                  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                        getContext());
                  LayoutInflater inflater = getActivity().getLayoutInflater();
                  View dialogView = inflater
                        .inflate(R.layout.multi_value_dailog_layout, null);
                  
                  dialogBuilder.setView(dialogView);
                  AlertDialog alertDialog = dialogBuilder.create();
                  dialogView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              alertDialog.dismiss();
                              
                        }
                  });
//                  LinearLayout degrees = dialogView.findViewById(R.id.degrees);
//                  for (int i = 0; i < degrees.getChildCount(); i++) {
//                        Button button = (Button) degrees.getChildAt(i);
//                        button.setOnClickListener(
//                              new OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                          vibrator.vibrate(30);
//                                          Device device = new Device();
//                                          device.setControllerId(room);
//                                          device.setDeviceId(
//                                                devices.get(position).getDeviceId());
//                                          device.setStatus(Integer
//                                                .parseInt(button.getText().toString()));
//                                          final String message = new Gson().toJson(
//                                                new TcpMessage() {{
//                                                      setCommand(TCP_COMMANDS.SET);
//                                                      setDevice(device);
//                                                }}, TcpMessage.class
//                                          );
//                                          TcpClient.send(getContext(), message);
//
//                                          new CountDownTimer(1000, 100) {
//
//                                                public void onTick(
//                                                      long millisUntilFinished) {
//                                                      for (int i = 0; i < degrees.getChildCount();
//                                                            i++) {
//                                                            Button button = (Button) degrees
//                                                                  .getChildAt(i);
//                                                            if (devices.get(position).getStatus()
//                                                                  == Integer.parseInt(((Button) v)
//                                                                  .getText().toString())) {
//                                                                  button.setTextColor(
//                                                                        Color.WHITE);
//                                                            } else {
//                                                                  button.setTextColor(
//                                                                        Color.BLACK);
//                                                            }
//                                                      }
//                                                }
//
//                                                public void onFinish() {
//
//                                                }
//                                          }.start();
//                                    }
//                              });
//                  }
//
                  
                  LinearLayout up = dialogView.findViewById(R.id.up);
                  LinearLayout down = dialogView.findViewById(R.id.down);
                  
                  TextView txtValue = dialogView.findViewById(R.id.txtValue);
                  ProgressBar progressBar = dialogView.findViewById(R.id.circle_progress_bar);
                  new CountDownTimer(1000, 100) {
                        
                        public void onTick(
                              long millisUntilFinished) {
                              progressBar.setProgress(devices.get(position).getStatus() * 10);
                              txtValue.setText(devices.get(position).getStatus() * 10 + "");
                        }
                        
                        public void onFinish() {
                        
                        }
                  }.start();
                  up.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              Device device = new Device();
                              device.setControllerId(room);
                              device.setDeviceId(devices.get(position).getDeviceId());
                              device.setStatus(
                                    Integer.parseInt(txtValue.getText().toString()) + 10);
                              device.setStatus(1);
                              final String message = new Gson().toJson(
                                    new TcpMessage() {{
                                          setCommand(TCP_COMMANDS.SET);
                                          setDevice(device);
                                    }}, TcpMessage.class
                              );
                              TcpClient.send(getContext(), message);
                              new CountDownTimer(1000, 100) {
                                    
                                    public void onTick(
                                          long millisUntilFinished) {
                                          progressBar.setProgress(
                                                devices.get(position).getStatus() * 10);
                                          txtValue.setText(
                                                devices.get(position).getStatus() * 10 + "");
                                    }
                                    
                                    public void onFinish() {
                                    
                                    }
                              }.start();
                              
                        }
                  });
                  down.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              Device device = new Device();
                              device.setControllerId(room);
                              device.setDeviceId(devices.get(position).getDeviceId());
                              device.setStatus(
                                    Integer.parseInt(txtValue.getText().toString()) - 10);
                              device.setStatus(0);
                              final String message = new Gson().toJson(
                                    new TcpMessage() {{
                                          setCommand(TCP_COMMANDS.SET);
                                          setDevice(device);
                                    }}, TcpMessage.class
                              );
                              TcpClient.send(getContext(), message);
                              new CountDownTimer(1000, 100) {
                                    
                                    public void onTick(
                                          long millisUntilFinished) {
                                          progressBar.setProgress(
                                                devices.get(position).getStatus() * 10);
                                          txtValue.setText(
                                                devices.get(position).getStatus() * 10 + "");
                                    }
                                    
                                    public void onFinish() {
                                    
                                    }
                              }.start();
                              
                        }
                  });
                  alertDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                  alertDialog.show();
                  alertDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(
                              android.graphics.Color.argb(100, 0, 0, 0))
                  );
                  alertDialog.getWindow()
                        .setLayout(SV.ScreenDimensions.getWidth(getActivity()),
                              SV.ScreenDimensions.getHeight(getActivity()));
            }
      }
      
      @Override
      public void onDeviceLongClicked(int position) {
            vibrator.vibrate(70);
            FragmentTransaction fragmentTransaction
                  = fragmentManager.beginTransaction();
            SettingsFragment settingsFragment = new SettingsFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("id", devices.get(position).getId());
            bundle.putLong("roomId",
                  devices.get(position).getControllerId());
            bundle.putLong("deviceKey", devices.get(position).getDeviceId());
            settingsFragment.setArguments(bundle);
            MainActivity.currentFragment = settingsFragment;
            fragmentTransaction.replace(R.id.main, settingsFragment)
                  .addToBackStack(null).commit();
      }
      
      
      @Override
      public void onDeviceReceived(Device device) {
            getReceivedDevice(device);
      }
}

