package team.morabaa.com.myudp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import team.morabaa.com.myudp.R;

/**
 * Created by eagle on 3/5/2018.
 */

public class Utils {
      
      public final static int THEME_DEFAULT = R.style.AppTheme;
      public final static int THEME_X = R.style.AppThemeX;
      public final static int THEME_BLUE = 2;
      private static int sTheme;

      public static Drawable getDrawableByName(Context context, String name) {
            int resourceId = context.getResources()
                  .getIdentifier(name, "drawable", context.getPackageName());
            return context.getResources().getDrawable(resourceId);
      }

      public static String getDrawableNameById(Context context, int resId) {
            return context.getResources().getResourceEntryName(resId);
      }
      
      /**
       * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
       */
      public static void changeToTheme(Activity activity, int theme) {
            sTheme = theme;
            activity.finish();
            activity.startActivity(new Intent(activity, activity.getClass()));
      }
      
      /**
       * Set the theme of the activity, according to the configuration.
       */
      public static void onActivityCreateSetTheme(Activity activity) {
            switch (sTheme) {
                  default:
                  case THEME_DEFAULT:
                        activity.setTheme(R.style.AppTheme);
                        break;
                  case THEME_X:
                        activity.setTheme(R.style.AppThemeX);
                        break;
            }
      }
}
