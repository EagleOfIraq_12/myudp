package team.morabaa.com.myudp.fragments;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import team.morabaa.com.myudp.R;
import team.morabaa.com.myudp.activities.MainActivity;
import team.morabaa.com.myudp.adapters.FloorAdapter;
import team.morabaa.com.myudp.model.Room;


public class HomeFragment extends Fragment {
      
      String floor;
      
      RecyclerView floorRecyclerView;
      String page;
      String ARG_PAGE;
      
      public HomeFragment() {
            // Required empty public constructor
      }
      
      public static ArrayList<View> getAllChildren(View v) {
            if (!(v instanceof ViewGroup)) {
                  ArrayList<View> viewArrayList = new ArrayList<>();
                  viewArrayList.add(v);
                  return viewArrayList;
            }
            
            ArrayList<View> result = new ArrayList<>();
            
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                  View child = vg.getChildAt(i);
                  ArrayList<View> viewArrayList = new ArrayList<View>();
                  viewArrayList.add(v);
                  viewArrayList.addAll(getAllChildren(child));
                  result.addAll(viewArrayList);
            }
            return result;
      }
      
      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
      }
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
//            if (savedInstanceState.containsKey(ARG_PAGE)) {
//                page = savedInstanceState.getString(ARG_PAGE);
//            }
//        }
            // Inflate the layout for this fragment
            floor = "000";
//        floor = getArguments().getString("floor");
            return inflater.inflate(R.layout.fragment_second, container, false);
      }
      
      @Override
      public void onViewCreated(View view, Bundle savedInstanceState) {
            getActivity().setTitle("الطابق " + "الاول");
            @SuppressLint("CommitTransaction")
            
            FragmentTransaction fragmentTransaction
                  = getActivity().getSupportFragmentManager().beginTransaction();
            
            ImageView btnFloorNo = view.findViewById(R.id.btnFloorNo);
            btnFloorNo.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.select_floor_layout, null);
                        dialogBuilder.setView(dialogView);
                        
                        String[] items = new String[]{"الاول", "الثاني"};
                        
                        final Spinner floorSpinner = dialogView.findViewById(R.id.floorSpinner);
                        
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                              getContext(), android.R.layout.simple_spinner_item,
                              items); //selected item will look like a spinner set from XML
                        spinnerArrayAdapter.setDropDownViewResource(
                              android.R.layout.simple_spinner_dropdown_item);
                        floorSpinner.setAdapter(spinnerArrayAdapter);
                        AlertDialog alertDialog = dialogBuilder.create();
                        dialogView.findViewById(R.id.btnOk)
                              .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          floor = floorSpinner.getSelectedItem().toString();
                                          Toast.makeText(getContext(), floor, Toast.LENGTH_SHORT)
                                                .show();
                                          alertDialog.dismiss();
                                          getActivity().setTitle("الطابق " + floor);
                                    }
                              });
                        
                        alertDialog.show();
                        alertDialog.getWindow().setBackgroundDrawable(
                              new ColorDrawable(android.graphics.Color.TRANSPARENT)
                        );
                        alertDialog.getWindow().setLayout(700, 600);
                        
                  }
            });
            List<Room> rooms = MainActivity.db.roomDuo().ROOMS();
            floorRecyclerView = view.findViewById(R.id.roomsGrid);
            floorRecyclerView.setAdapter(new FloorAdapter(getContext(), fragmentTransaction,
                  getActivity(),
                  rooms, "000"));
            
            if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
                  floorRecyclerView.setLayoutManager(
                        new GridLayoutManager(getContext(), 2)
                  );
                  
            } else {
                  floorRecyclerView.setLayoutManager(
                        new GridLayoutManager(getContext(), 2, 0, true)
                  );
            }
            
            for (View view1 : getAllChildren(view)) {
//            handleBackPress(view1);
            }
      }
      
      @Override
      public void onSaveInstanceState(Bundle outState) {
            outState.putString(ARG_PAGE, page);
            super.onSaveInstanceState(outState);
      }
      
      private void handleBackPress(View view) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(
                  new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                              if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    System.exit(0);
                                    return true;
                              }
                              return false;
                        }
                  }
            );
      }
}
