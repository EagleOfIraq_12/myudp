package team.morabaa.com.myudp.utils;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by eagle on 1/16/2018.
 */

public class SV {
      public static String DEVICE_ID="";
      public static String SERVER_IP ="";
      public static class TCP_COMMANDS {
            
            public static final String GET = "get";
            public static final String SET = "set";
            public static final String UPDATE_DEVICE = "updateDevice";
            public static final String ADD_DEVICE = "addDevice";
            public static final String REFRESH = "refresh";
            public static final String GET_DEVICES = "getDevices";
            public static final String GET_ROOMS = "getRooms";
            public static final String CHECK_SERVER = "areYouServer";
      }
      
      public static class ScreenDimensions {
            
            public static int getHeight(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.heightPixels;
            }
            
            public static int getWidth(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.widthPixels;
            }
      }
}
