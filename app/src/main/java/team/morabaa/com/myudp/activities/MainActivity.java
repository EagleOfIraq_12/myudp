package team.morabaa.com.myudp.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import java.util.List;
import team.morabaa.com.myudp.R;
import team.morabaa.com.myudp.fragments.HomeFragment;
import team.morabaa.com.myudp.interfaces.ActivityFragmentBridge;
import team.morabaa.com.myudp.model.Device;
import team.morabaa.com.myudp.model.FirebaseMessage;
import team.morabaa.com.myudp.model.Room;
import team.morabaa.com.myudp.model.TcpMessage;
import team.morabaa.com.myudp.tcp.TcpClient;
import team.morabaa.com.myudp.tcp.TcpServer;
import team.morabaa.com.myudp.utils.ConnectionState;
import team.morabaa.com.myudp.utils.DB;
import team.morabaa.com.myudp.utils.LocalData;
import team.morabaa.com.myudp.utils.SV;
import team.morabaa.com.myudp.utils.SV.TCP_COMMANDS;
import team.morabaa.com.myudp.utils.Utils;

/* 17-26
 * door 31
 * fan 20
 * temp 30
 * humidity 33
 * motion 32
 * buzzer 34 on off
 * buzzer 35 second
 * buzzer 36 start
 * buzzer 37 end
 */
public class MainActivity extends AppCompatActivity {
      
      public static final String DEVICE_ID = "DEVICE_ID";
      public static Fragment currentFragment;
      public static DatabaseReference fromPhoneMessageRef;
      public static DB db;
      public static Context ctx;
      public static Handler messageHandler;
      public static DatabaseReference devicesRef;
      public static DatabaseReference roomsRef;
      public static ValueEventListener fromControllerMessageListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                  FirebaseMessage firebaseMessage = dataSnapshot.getValue(FirebaseMessage.class);
                  System.out
                        .println("received from FireBase:\t" + firebaseMessage.getMessage());
                  String message = firebaseMessage.getMessage();

//                  if (messageHandler != null && !firebaseMessage.getBssid()
//                        .equals(ConnectionState.getBssid(ctx))) { // no need
                  System.out
                        .println("BSSIDs:\t" + firebaseMessage.getBssid() + "\t :"
                              + ConnectionState
                              .getBssid(ctx)
                        );
                  
                  Bundle bundle = new Bundle();
                  bundle.putString("result", message);
                  Message msg = new Message();
                  msg.setData(bundle);
                  messageHandler.sendMessage(msg);
//                  }
            }
            
            @Override
            public void onCancelled(DatabaseError databaseError) {
                  
            }
      };
      static DatabaseReference fromControllerMessageRef;
      static ValueEventListener devicesValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                  db.deviceDuo().deleteAll();
                  for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Device device = snapshot.getValue(Device.class);
                        db.deviceDuo().insert(device);
                  }
                  TcpMessage tcpMessage = new TcpMessage();
                  tcpMessage.setCommand(TCP_COMMANDS.GET_DEVICES);
                  TcpClient
                        .send(ctx, new Gson().toJson(tcpMessage, TcpMessage.class));
            }
            
            @Override
            public void onCancelled(DatabaseError databaseError) {
                  db.deviceDuo().deleteAll();
                  TcpMessage tcpMessage = new TcpMessage();
                  tcpMessage.setCommand(TCP_COMMANDS.GET_DEVICES);
                  TcpClient
                        .send(ctx, new Gson().toJson(tcpMessage, TcpMessage.class));
            }
      };
      static ValueEventListener roomsValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                  db.roomDuo().deleteAll();
                  for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Room room = snapshot.getValue(Room.class);
                        db.roomDuo().insert(room);
                  }
                  TcpMessage tcpMessage = new TcpMessage();
                  tcpMessage.setCommand(TCP_COMMANDS.GET_ROOMS);
                  TcpClient
                        .send(ctx, new Gson().toJson(tcpMessage, TcpMessage.class));
            }
            
            @Override
            public void onCancelled(DatabaseError databaseError) {
                  db.roomDuo().deleteAll();
                  TcpMessage tcpMessage = new TcpMessage();
                  tcpMessage.setCommand(TCP_COMMANDS.GET_ROOMS);
                  TcpClient
                        .send(ctx, new Gson().toJson(tcpMessage, TcpMessage.class));
            }
      };
      FirebaseDatabase database = FirebaseDatabase.getInstance();
      DatabaseReference firebaseReference;
      DatabaseReference placeNameRef;
      ActivityFragmentBridge activityFragmentBridge;
      int themeI = 0;
      
      public static List<Room> getRooms(Context ctx) {
            db.roomDuo().deleteAll();
            
            db.roomDuo().insert(new Room() {{
                  setId(117);
                  setControllerId(117);
                  setName("المعرض");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(118);
                  setControllerId(118);
                  setName("موقع 118");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(119);
                  setControllerId(119);
                  setName("موقع 119");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(120);
                  setControllerId(120);
                  setName("موقع 120");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(121);
                  setControllerId(121);
                  setName("موقع 121");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(122);
                  setControllerId(122);
                  setName("موقع 122");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(123);
                  setControllerId(123);
                  setName("موقع 123");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(124);
                  setControllerId(124);
                  setName("موقع 124");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(125);
                  setControllerId(125);
                  setName("موقع 125");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            db.roomDuo().insert(new Room() {{
                  setId(126);
                  setControllerId(126);
                  setName("موقع 126");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
                  setFloorId("1");
            }});
            
            return db.roomDuo().ROOMS();
      }

//      public static List<Device> getDevices(final Context ctx) {
//            db.deviceDuo().deleteAll();
//            for (int i = 117; i < 127; i++) {
//                  final int finalI = i;
//                  for (int j = 1; j < 8; j++) {
//                        final int finalJ = j;
//                        db.deviceDuo().insert(new Device() {{
//                              setId(Long
//                                    .parseLong(finalI + String.format("%30d", finalJ, Locale.US)));
//                              setDeviceId(finalJ);
//                              setControllerId(finalI);
//                              setName("إضاءه " + finalJ);
//                              setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
//                              setRoomId(finalI);
//                        }});
//                  }
//                  db.deviceDuo().insert(new Device() {{
//                        setId(Long.parseLong(finalI + String.format("%3d0", 20, Locale.US)));
//                        setDeviceId(20);
//                        setControllerId(finalI);
//                        setName("مروحه 6");
//                        setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
//                        setRoomId(finalI);
//                  }});
//            }
//
//            StringBuilder s = new StringBuilder("[");
//            for (Device device : db.deviceDuo().DEVICES()) {
//                  s.append(new Gson().toJson(device, Device.class)).append(",");
//
//            }
//            s.append("]");
//            System.out.println("Devices ...  " + s);
//
//            return db.deviceDuo().DEVICES();
//      }
      
      public static List<Device> getDevices(Context ctx) {
            db.deviceDuo().deleteAll();
            
            db.deviceDuo().insert(new Device() {{
                  setId(117001);
                  setDeviceId(1);
                  setControllerId(117);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(117002);
                  setDeviceId(2);
                  setControllerId(117);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(117003);
                  setDeviceId(3);
                  setControllerId(117);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(117004);
                  setDeviceId(4);
                  setControllerId(117);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(117005);
                  setDeviceId(5);
                  setControllerId(117);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(117006);
                  setDeviceId(6);
                  setControllerId(117);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(117020);
                  setDeviceId(20);
                  setControllerId(117);
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(118001);
                  setDeviceId(1);
                  setControllerId(118);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(118002);
                  setDeviceId(2);
                  setControllerId(118);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(118003);
                  setDeviceId(3);
                  setControllerId(118);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(118004);
                  setDeviceId(4);
                  setControllerId(118);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(118005);
                  setDeviceId(5);
                  setControllerId(118);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(118006);
                  setDeviceId(6);
                  setControllerId(118);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(117);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(118020);
                  setDeviceId(20);
                  setControllerId(118);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(117);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(119001);
                  setDeviceId(1);
                  setControllerId(119);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(119);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(119002);
                  setDeviceId(2);
                  setControllerId(119);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(119);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(119003);
                  setDeviceId(3);
                  setControllerId(119);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(119);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(119004);
                  setDeviceId(4);
                  setControllerId(119);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(119);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(119005);
                  setDeviceId(5);
                  setControllerId(119);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(119);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(119006);
                  setDeviceId(6);
                  setControllerId(119);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(119);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(119020);
                  setDeviceId(20);
                  setControllerId(119);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(119);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(120001);
                  setDeviceId(1);
                  setControllerId(120);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(120);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120002);
                  setDeviceId(2);
                  setControllerId(120);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(120);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120003);
                  setDeviceId(3);
                  setControllerId(120);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(120);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120004);
                  setDeviceId(4);
                  setControllerId(120);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(120);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120005);
                  setDeviceId(5);
                  setControllerId(120);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(120);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120006);
                  setDeviceId(6);
                  setControllerId(120);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(120);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120020);
                  setDeviceId(20);
                  setControllerId(120);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(120);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(121001);
                  setDeviceId(1);
                  setControllerId(121);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(121);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(121002);
                  setDeviceId(2);
                  setControllerId(121);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(121);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(121003);
                  setDeviceId(3);
                  setControllerId(121);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(121);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(121004);
                  setDeviceId(4);
                  setControllerId(121);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(121);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(121005);
                  setDeviceId(5);
                  setControllerId(121);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(121);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(121006);
                  setDeviceId(6);
                  setControllerId(121);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(121);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(120020);
                  setDeviceId(20);
                  setControllerId(120);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(120);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(122001);
                  setDeviceId(1);
                  setControllerId(122);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(122);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(122002);
                  setDeviceId(2);
                  setControllerId(122);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(122);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(122003);
                  setDeviceId(3);
                  setControllerId(122);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(122);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(122004);
                  setDeviceId(4);
                  setControllerId(122);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(122);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(122005);
                  setDeviceId(5);
                  setControllerId(122);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(122);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(122006);
                  setDeviceId(6);
                  setControllerId(122);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(122);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(122020);
                  setDeviceId(20);
                  setControllerId(122);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(122);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(123001);
                  setDeviceId(1);
                  setControllerId(123);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(123);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(123002);
                  setDeviceId(2);
                  setControllerId(123);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(123);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(123003);
                  setDeviceId(3);
                  setControllerId(123);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(123);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(123004);
                  setDeviceId(4);
                  setControllerId(123);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(123);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(123005);
                  setDeviceId(5);
                  setControllerId(123);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(123);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(123006);
                  setDeviceId(6);
                  setControllerId(123);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(123);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(123020);
                  setDeviceId(20);
                  setControllerId(123);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(123);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(124001);
                  setDeviceId(1);
                  setControllerId(124);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(124);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(124002);
                  setDeviceId(2);
                  setControllerId(124);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(124);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(124003);
                  setDeviceId(3);
                  setControllerId(124);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(124);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(124004);
                  setDeviceId(4);
                  setControllerId(124);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(124);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(124005);
                  setDeviceId(5);
                  setControllerId(124);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(124);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(124006);
                  setDeviceId(6);
                  setControllerId(124);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(124);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(124020);
                  setDeviceId(20);
                  setControllerId(124);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(124);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(125001);
                  setDeviceId(1);
                  setControllerId(125);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(125);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(125002);
                  setDeviceId(2);
                  setControllerId(125);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(125);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(125003);
                  setDeviceId(3);
                  setControllerId(125);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(125);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(125004);
                  setDeviceId(4);
                  setControllerId(125);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(125);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(125005);
                  setDeviceId(5);
                  setControllerId(125);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(125);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(125006);
                  setDeviceId(6);
                  setControllerId(125);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(125);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(125020);
                  setDeviceId(20);
                  setControllerId(125);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(125);
            }});
            
            db.deviceDuo().insert(new Device() {{
                  setId(126001);
                  setDeviceId(1);
                  setControllerId(126);
                  setName("إضاءه 1");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(126);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(126002);
                  setDeviceId(2);
                  setControllerId(126);
                  setName("إضاءه 2");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(126);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(126003);
                  setDeviceId(3);
                  setControllerId(126);
                  setName("إضاءه 3");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(126);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(126004);
                  setDeviceId(4);
                  setControllerId(126);
                  setName("إضاءه 4");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(126);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(126005);
                  setDeviceId(5);
                  setControllerId(126);
                  setName("إضاءه 5");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(126);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(126006);
                  setDeviceId(6);
                  setControllerId(126);
                  setName("إضاءه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
                  setRoomId(126);
            }});
            db.deviceDuo().insert(new Device() {{
                  setId(126020);
                  setDeviceId(20);
                  setControllerId(126);
                  setType("multiValue");
                  setName("مروحه 6");
                  setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_b));
                  setRoomId(126);
            }});
            
            StringBuilder s = new StringBuilder("[");
            for (Device device : db.deviceDuo().DEVICES()) {
                  s.append(new Gson().toJson(device, Device.class)).append(",");
                  
            }
            s.append("]");
            System.out.println("Devices ...  " + s);
            
            return db.deviceDuo().DEVICES();
      }
      
      public static void indoor(boolean b) {
            if (!b) {
                  fromControllerMessageRef.addValueEventListener(fromControllerMessageListener);
            } else {
                  fromControllerMessageRef.removeEventListener(fromControllerMessageListener);
            }
      }
      
      private Handler TcpMessageRouter() {
            return new Handler(Looper.getMainLooper()) {
                  @SuppressLint("SetTextI18n")
                  public void handleMessage(Message msg) {
                        themeI++;
//                        setTheme() ;
                        assert msg != null;
                        String message = msg.getData().getString("result");
                        TcpMessage tcpMessage = new Gson().fromJson(message, TcpMessage.class);
                        String deviceString = new Gson().toJson(
                              tcpMessage.getDevice(), Device.class
                        );
                        final Device device = new Gson()
                              .fromJson(deviceString, Device.class);
                        
                        if (tcpMessage.getDeviceId().equals(SV.DEVICE_ID)) {
                              switch (tcpMessage.getCommand()) {
                                    case TCP_COMMANDS.SET: {
                                          activityFragmentBridge.onDeviceReceived(device);
                                          break;
                                    }
                                    case TCP_COMMANDS.UPDATE_DEVICE: {
                                          db.deviceDuo().Update(device);
                                          Device currentDevice = db.deviceDuo()
                                                .getDeviceByID(device.getId());
                                          System.out.println("updated:" +
                                                "\n" + new Gson().toJson(currentDevice));
                                          break;
                                    }
                                    case TCP_COMMANDS.ADD_DEVICE: {
                                          
                                          //                                    db.deviceDuo().insert(device);
                                          //                                    Device currentDevice = db.deviceDuo()
                                          //                                          .getDeviceByID(device.getId());
                                          //                                    System.out.println("inserted:" +
                                          //                                          "\n" + new Gson().toJson(currentDevice));
                                          break;
                                    }
                              }
                        }
                  }
            };
      }
      
      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Utils.onActivityCreateSetTheme(this);
            setContentView(R.layout.main);
            messageHandler = TcpMessageRouter();
            ctx = MainActivity.this;
            db = android.arch.persistence.room.Room.databaseBuilder(
                  MainActivity.this, DB.class,
                  "smartHome.db"
            )
                  .allowMainThreadQueries()
                  .fallbackToDestructiveMigration()
                  .build();
            
            getRooms(MainActivity.this);
            getDevices(MainActivity.this);
            
            if (LocalData.get(MainActivity.this, DEVICE_ID).length() > 0) {
                  goOn();
            } else {
                  IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                  integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                  integrator.setPrompt("Scan Code");
                  integrator.setCameraId(0);
                  integrator.setBeepEnabled(true);
                  integrator.setBarcodeImageEnabled(false);
                  integrator.initiateScan();
            }
            test();
            
      }
      
      private void test() {
            String name = db.roomDuo().getRoomNameByID(1);
            
            List<String> names = db.roomDuo().getRoomsNames();
            StringBuilder s = new StringBuilder("name \t" + name + "\n");
            int i = 1;
            for (String s1 : names) {
                  long id = db.roomDuo().getRoomIDByName(s1);
                  s.append("name ").append(id).append(" \t").append(s1).append("\n");
            }
            System.out.println(s);
      }
      
      @Override
      protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            IntentResult intentResult = IntentIntegrator
                  .parseActivityResult(requestCode, resultCode, data);
            if (intentResult != null) {
                  if (intentResult.getContents() == null) {
                        Log.d("MainActivity", "Cancelled");
                        Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                        
                  } else {
                        Log.d("MainActivity", "Scanned");
                        String contents = intentResult.getContents();
                        TcpMessage tcpMessage = new Gson().fromJson(contents, TcpMessage.class);
                        String key = "";
                        if (tcpMessage.getCommand().equals("key")) {
                              key = tcpMessage.getDevice().getName();
                        }
                        Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
                        resultIntent.putExtra("key", key);
                        LocalData.add(MainActivity.this, DEVICE_ID, key);
                        startActivity(resultIntent);
                  }
            }
      }
      
      private void goOn() {
            SV.DEVICE_ID = LocalData.get(MainActivity.this, DEVICE_ID);
            firebaseReference = database.getReference(LocalData.get(MainActivity.this, DEVICE_ID));
            fromControllerMessageRef = firebaseReference.child("fromControllerMessage");
            roomsRef = firebaseReference.child("rooms");
            devicesRef = firebaseReference.child("devices");
            fromPhoneMessageRef = firebaseReference.child("fromPhoneMessageRef");
            
            placeNameRef = firebaseReference.child("name");
//            roomsRef.addListenerForSingleValueEvent(roomsValueEventListener);
//            devicesRef.addListenerForSingleValueEvent(devicesValueEventListener);
            new TcpServer().run();
            indoor(true);
            
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                  .beginTransaction();
            HomeFragment homeFragment = new HomeFragment();
            Bundle bundle = new Bundle();
            bundle.putString("floor", "000");
            homeFragment.setArguments(bundle);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                  .replace(R.id.main, homeFragment)
                  .addToBackStack(null)
                  .commit();
      }
      
      public void refresh(View view) {
            TcpMessage tcpMessage = new TcpMessage();
            tcpMessage.setCommand(TCP_COMMANDS.REFRESH);
            TcpClient.send(MainActivity.this, new Gson().toJson(tcpMessage, TcpMessage.class));
      }
      
      public void getDevices(View view) {
            TcpMessage tcpMessage = new TcpMessage();
            tcpMessage.setCommand(TCP_COMMANDS.GET_DEVICES);
            TcpClient.send(MainActivity.this,
                  new Gson().toJson(tcpMessage, TcpMessage.class));
      }
      
      public void setActivityFragmentBridge(ActivityFragmentBridge activityFragmentBridge) {
            this.activityFragmentBridge = activityFragmentBridge;
      }
      
      void setTheme() {
            
            if (themeI % 2 != 0) {
                  Utils.changeToTheme(this, Utils.THEME_DEFAULT);
            } else {
                  Utils.changeToTheme(this, Utils.THEME_X);
            }
            
      }
//      @Override
//      public Resources.Theme getTheme() {
//            Resources.Theme theme = super.getTheme();
//            if (themeI % 2 == 0) {
//                  theme.applyStyle(R.style.AppThemeX, true);
//            } else {
//                  theme.applyStyle(R.style.AppTheme, true);
//            }
//            // you could also use a switch if you have many themes that could apply
//            return theme;
//      }
}