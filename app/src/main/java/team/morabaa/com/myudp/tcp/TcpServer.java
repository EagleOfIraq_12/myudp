package team.morabaa.com.myudp.tcp;


import static team.morabaa.com.myudp.activities.MainActivity.messageHandler;
import static team.morabaa.com.myudp.utils.SV.SERVER_IP;

import android.os.Bundle;
import android.os.Message;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * Created by eagle on 2/11/2018.
 */
public class TcpServer {
      
      
      public TcpServer() {
      }
      
      public void run() {
            new Thread(new Runnable() {
                  @Override
                  public void run() {
                        String incomingMsg = "";
                        try {
                              System.out.println("Starting socket thread...");
                              ServerSocket serverSocket = new ServerSocket(21112);
                              System.out
                                    .println(
                                          "ServerSocket created, waiting for incoming connections......");
                              while (true) {
                                    
                                    Socket socket = serverSocket.accept();
                                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                                          socket.getOutputStream()));
                                    SERVER_IP=socket.getInetAddress().getHostAddress();
                                    byte[] buffer = new byte[1400];
                                    int r = socket.getInputStream().read(buffer, 0, buffer.length);
                                    incomingMsg = new String(buffer);
                                    incomingMsg = incomingMsg
                                          .substring(0, incomingMsg.lastIndexOf("}") + 1);
                                    System.out.println("Message received from Tcp: " + incomingMsg);
      
                                    int i = 0;
                                    while (socket.isConnected() && i == 0) {
                                          if (messageHandler != null) {
                                                Bundle bundle = new Bundle();
                                                bundle.putString("result", incomingMsg);
                                                Message msg = new Message();
                                                msg.setData(bundle);
                                                messageHandler.sendMessage(msg);
                                          }
                                          
                                          // send a message
                                          String outgoingMsg =
                                                "OK \t" + new Date().toString();
                                          out.write(outgoingMsg);
                                          out.flush();
                                          System.out.println("Message sent: " + outgoingMsg);
                                          socket.close();
                                          i = 1;
                                    }
                                    
                              }
                              
                        } catch (Exception e) {
                              System.out.println("Error: " + e.getMessage());
                              e.printStackTrace();
                        }
                  }
            }).start();
      }
}