package team.morabaa.com.myudp.model;

import team.morabaa.com.myudp.utils.SV;

/**
 * Created by eagle on 3/1/2018.
 */

public class TcpMessage {
      
      private String command;
      private Device device;
      private String DeviceId = SV.DEVICE_ID;
      
      public TcpMessage() {
      }
      
      public String getCommand() {
            return command;
      }
      
      public void setCommand(String command) {
            this.command = command;
      }
      
      public Device getDevice() {
            return device;
      }
      
      public void setDevice(Device device) {
            this.device = device;
      }
      
      public String getDeviceId() {
            return DeviceId;
      }
      
      public void setDeviceId(String deviceId) {
            DeviceId = deviceId;
      }
}
