package team.morabaa.com.myudp;

/**
 * Created by eagle on 10/29/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * -Created by TutorialsPoint7 on 8/23/2016.
 */
public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String result = intent.getStringExtra("result");
        intent.removeExtra("result");

//        Bundle bundle=new Bundle();
//        bundle.putString("result",result);
//        Message message=new Message();
//        message.setData(bundle);
//        messageHandler.sendMessage(message);
//        RoomAdapter.txtLastMessage.setText(result);
    }


    private static void sendToBroadcast(String result,Context ctx) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        intent.setAction("team.morabaa.com.myudp.CUSTOM_INTENT");
        ctx.sendBroadcast(intent);
    }
    /*
    mainifests

     <receiver
            android:name=".MyReceiver"
            android:permission="">
            <intent-filter>
                <action android:name="team.morabaa.com.myudp.CUSTOM_INTENT" />
            </intent-filter>
        </receiver>
    */
}
