package team.morabaa.com.myudp.fragments;


import static team.morabaa.com.myudp.activities.MainActivity.db;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import com.google.gson.Gson;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import team.morabaa.com.myudp.R;
import team.morabaa.com.myudp.model.Device;
import team.morabaa.com.myudp.model.Room;
import team.morabaa.com.myudp.model.TcpMessage;
import team.morabaa.com.myudp.tcp.TcpClient;
import team.morabaa.com.myudp.utils.SV.TCP_COMMANDS;

public class SettingsFragment extends Fragment {
      
      String floorId;
      long roomId;
      Button btnSave;
      TextView txtDeviceName;
      TextView txtControllerId;
      TextView txtRoomId;
      TextView txtDeviceKey;
      Spinner spinnerRoomKey;
      Spinner spinnerType;
      Switch toggleOnLowVoltage;
      LinearLayout imagesContainer;
      int imageId;
      Device device;
      long id;
      
      public SettingsFragment() {
            // Required empty public constructor
      }
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            System.out.println("Device id: " + getArguments().getLong("id"));
            floorId = getArguments().getString("floorId");
            roomId = getArguments().getLong("roomId");
            id = getArguments().getLong("id");
            device = db.deviceDuo().getDeviceByID(id);
            
            return inflater.inflate(R.layout.fragment_settings, container, false);
      }
      
      @SuppressLint("SetTextI18n")
      @Override
      public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            
            txtRoomId = view.findViewById(R.id.txtRoomId);
            txtControllerId = view.findViewById(R.id.txtControllerId);
            btnSave = view.findViewById(R.id.btnSave);
            txtDeviceName = view.findViewById(R.id.txtDeviceName);
            txtDeviceKey = view.findViewById(R.id.txtDeviceKey);
            spinnerRoomKey = view.findViewById(R.id.spinnerRoomKey);
            List<String> roomsKeys = new ArrayList<>();
            for (Room room : db.roomDuo().ROOMS()) {
                  roomsKeys.add(room.getName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                  android.R.layout.simple_spinner_item, roomsKeys);
            spinnerRoomKey.setAdapter(adapter);
            spinnerType = view.findViewById(R.id.spinnerType);
            imagesContainer = view.findViewById(R.id.imagesContainer);
            toggleOnLowVoltage = view.findViewById(R.id.toggleOnLowVoltage);
            
            if (device == null) {
                  device = new Device();
            } else {
                  try {
                        txtControllerId.setText(device.getControllerId() + "");
                        txtRoomId.setText(device.getRoomId() + "");
                        txtDeviceName.setText(device.getName());
                        txtDeviceKey.setText(device.getDeviceId() + "");
                        spinnerRoomKey.setPrompt(device.getControllerId() + " ...");
                        spinnerType.setPrompt(device.getType());
                  } catch (Exception ignored) {
                  }
            }
            
            Field[] ints = R.drawable.class.getFields();
            List<CardView> imageContainers = new ArrayList<>();
            for (Field field : ints) {
                  CardView imageContainer = (CardView) getLayoutInflater()
                        .inflate(R.layout.device_image, null);
//                  fixSize(imageContainer);
                  ImageView imageView = imageContainer.findViewById(R.id.imgIcon);
                  try {
                        if (field.getName().startsWith("device_")) {
                              imageView.setImageDrawable(
                                    getContext().getResources().getDrawable(field.getInt(null)));
                              imagesContainer.addView(imageContainer);
                              imageContainer.setId(field.getInt(null));
                              imageContainers.add(imageContainer);
                        }
                  } catch (IllegalAccessException e) {
                        e.printStackTrace();
                  }
            }
            
            for (CardView imageContainer : imageContainers) {
                  Drawable background = imageContainer.getBackground();
                  imageContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              for (CardView ic : imageContainers) {
                                    if (v == ic) {
                                          imageId = imageContainer.getId();
                                          ic.setBackgroundColor(Color.parseColor("#33340079"));
                                    } else {
                                          ic.setBackground(background);
                                    }
                              }
                        }
                  });
            }
            btnSave.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        device.setIconName(
                              getContext().getResources().getResourceEntryName(imageId)
                        );
//                        device.setIconName("devise_e");
                        device.setName(txtDeviceName.getText().toString());
                        device.setDeviceId(Long.parseLong(txtDeviceKey.getText().toString()));
                        device.setRoomId(db.roomDuo()
                              .getRoomIDByName(spinnerRoomKey.getSelectedItem().toString()));
//                        device.setControllerId(
//                              Long.parseLong(spinnerRoomKey.getSelectedItem().toString()));
//                        device.setControllerId(roomId);
                        device.setId(id);
//                        device.setId(Long.parseLong(roomId.substring(1)+txtDeviceKey.getText().toString().substring(1)));
                        if (spinnerType.getSelectedItem().toString().equals("0-1")) {
                              device.setType("single");
                        } else {
                              device.setType("multiValue");
                        }
                        System.out.println("Device Id: \t" + device.getId());
                        
                        if (device.getId() != 0) {
                              System.out.println("Updated device Id: \t" + device.getId());
//                              MainActivity.db.deviceDuo().Update(device);
                        } else {
                              System.out.println("Inserted device Id: \t" + device.getId());
                              db.deviceDuo().insert(device);
                        }
                        //.....
                        TcpMessage tcpMessage = new TcpMessage();
                        tcpMessage.setCommand(TCP_COMMANDS.UPDATE_DEVICE);
                        tcpMessage.setDevice(device);
                        TcpClient
                              .send(getContext(), new Gson().toJson(tcpMessage, TcpMessage.class));
                        getFragmentManager().popBackStack();
                  }
            });
      }
      
      private void fixSize(final View view) {
            if (view != null) {
                  view.getViewTreeObserver().addOnPreDrawListener(
                        new ViewTreeObserver.OnPreDrawListener() {
                              @Override
                              public boolean onPreDraw() {
                                    int w = (int) (view.getMeasuredHeight() * 0.5f);
                                    view.getViewTreeObserver().removeOnPreDrawListener(this);
                                    view.getLayoutParams().width = w;
                                    view.getLayoutParams().height = w;
                                    view.requestLayout();
                                    return false;
                              }
                        });
            }
      }
}
