package team.morabaa.com.myudp.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import team.morabaa.com.myudp.activities.MainActivity;
import team.morabaa.com.myudp.R;
import team.morabaa.com.myudp.fragments.RoomFragment;
import team.morabaa.com.myudp.model.Room;
import team.morabaa.com.myudp.utils.SV;

/**
 * Created by eagle on 11/2/2017.
 */

public class FloorAdapter extends RecyclerView.Adapter<FloorViewHolder> {
    List<Room> rooms;
    Context ctx;
    String floorId;
    FragmentTransaction fragmentTransaction;
    Activity mainActivity;

    public FloorAdapter(Context ctx,
                        FragmentTransaction fragmentTransaction,
                        Activity mainActivity,
                        List<Room> rooms, String floorId) {
        this.rooms = rooms;
        this.ctx = ctx;
        this.mainActivity = mainActivity;
        this.fragmentTransaction = fragmentTransaction;
        this.floorId = floorId;
    }

    @Override
    public FloorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.thing_o, parent, false);

        return new FloorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FloorViewHolder holder, int position) {

        fixSize(holder.view);
        holder.textView.setText(rooms.get(holder.getAdapterPosition()).getName());
        holder.imageView.setImageDrawable(
              ctx.getResources().getDrawable(
                    ctx.getResources()
                          .getIdentifier(rooms.get(holder.getAdapterPosition()).getIconName(), "drawable",
                                ctx.getPackageName())
              )
        );
        holder.view.setOnClickListener(
                new View.OnClickListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onClick(View view) {
                        RoomFragment roomFragment = new RoomFragment();
                        Bundle bundle = new Bundle();
                        int f = Integer.parseInt(floorId);
                        f = f % 100;
                        if (f < 10)
                            floorId = "0" + f;
                        else
                            floorId = String.valueOf(f);
                        bundle.putLong("room", rooms.get(holder.getAdapterPosition()).getControllerId());
                        bundle.putString("floor", floorId);
                        roomFragment.setArguments(bundle);
                        MainActivity.currentFragment= roomFragment;
                        fragmentTransaction.replace(R.id.main, roomFragment)
                                .addToBackStack(null).commit();
                    }
                });

    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    private void fixSize(View view) {
        if (view != null) {
            int dim = (int) (SV.ScreenDimensions.getWidth(mainActivity) / 2.25);
            view.getLayoutParams().width = dim;
            view.getLayoutParams().height = dim;
            view.requestLayout();

        }
    }
}

class FloorViewHolder extends RecyclerView.ViewHolder {
    CardView view;
    TextView textView;
    ImageView imageView;
    LinearLayout out;

    public FloorViewHolder(View itemView) {
        super(itemView);
        view = (CardView) itemView;
        out = itemView.findViewById(R.id.out);
        imageView = itemView.findViewById(R.id.imgIcon);
        textView = itemView.findViewById(R.id.txtName);
    }
}