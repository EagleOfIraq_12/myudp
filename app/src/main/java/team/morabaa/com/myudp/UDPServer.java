package team.morabaa.com.myudp;

/**
 * -Created by eagle on 10/15/2017.
 */


import static team.morabaa.com.myudp.activities.MainActivity.messageHandler;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;

public class UDPServer {
      
      public static String getLocalIpAddress() {
            try {
                  for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                        en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
                              enumIpAddr.hasMoreElements(); ) {
                              InetAddress inetAddress = enumIpAddr.nextElement();
                              if (!inetAddress.isLoopbackAddress()) {
                                    InetAddress address = inetAddress;
                                    return intf.getInterfaceAddresses().get(1).getAddress()
                                          .getHostName();
//                                    return address.getHostAddress();
                              }
                        }
                  }
            } catch (SocketException ex) {
                  Log.e("ip", ex.toString());
            }
            return null;
      }
      
      public static void sendUdp(final String msg) {
            
            new Thread(
                  new Runnable() {
                        @Override
                        public void run() {
                              String myIp = getLocalIpAddress();
//                              192 . 168 . 0 . 125
//                              012 3 456 7 8 9 012
                              System.out.println("myIp: " + myIp);
                              try {
                                    DatagramSocket socket;
                                    String ip = "192.168.0.60";
                                    for (int i = 1; i < 256; i++) {
                                          ip = myIp.substring(0, 10) + i;
                                          InetAddress serverAddr = InetAddress.getByName(ip);
                                          System.out.println("C: Connecting...");
                                          socket = new DatagramSocket();
                                          byte[] buf = new byte[18];
                                          buf = msg.getBytes();
                                          System.out.println("bytes: " + Arrays.toString(buf));
                                          int portSend = 60000;
                                          DatagramPacket packet =
                                                new DatagramPacket(buf, buf.length, serverAddr,
                                                      portSend);
                                          System.out.println(
                                                "C: Sending: " + new String(buf) + " to " + ip
                                          );
                                          socket.setSoTimeout(1500);
                                          socket.send(packet);
                                          System.out.println("C: Sent.");
                                          System.out.println("C: Done.");
                                          socket.close();
                                    }
                              } catch (IOException e) {
                                    System.out
                                          .println("C:SocketException: Error " + e.getMessage());
                              }
                        }
                  }
            ).start();
      }
      
      
      public static void run() {
            DatagramSocket socket;
            try {
                  int port = 60001;
                  socket = new DatagramSocket(port);
                  byte[] pacData;
                  DatagramPacket packet;
                  pacData = new byte[18];
                  packet = new DatagramPacket(pacData, pacData.length);
                  while (true) {
                        try {
//                    if (RoomFragment.on) {  //.. run in background
                              socket.setSoTimeout(1500);
                              
                              socket.receive(packet);
                              String result = "";
                              try {
                                    result = new String(packet.getData(), "UTF-8");
                              } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                              }
                              socket.close();
                              Bundle bundle = new Bundle();
                              bundle.putString("result", result);

//                              String finalResult = result;
//                              new Thread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                          try {
//                                                for (UDPMessage udpMessage : udpMessages) {
//                                                      if (udpMessage.getBody()
//                                                            .equals(finalResult.substring(2, 13))) {
//                                                            udpMessages.remove(udpMessage);
//                                                            System.out.println(
//                                                                  "udp: remove " + udpMessages
//                                                                        .size());
//                                                      }
//                                                }
//                                          } catch (ConcurrentModificationException e) {
//                                                udpMessages = new ArrayList<>();
//                                          }
//                                    }
//                              }).start();
                              
                              if (messageHandler != null) {
                                    Message message = new Message();
                                    message.setData(bundle);
                                    messageHandler.sendMessage(message);
                              }
//                    } //.. run in background
                              run();
                        } catch (IOException e) {
//                    System.out.println("C: Run Error Could be due to timeout. " + e.getMessage());
                        }
                  }
            } catch (SocketException e) {
//            System.out.println("C:Error: Could not create socket on port " + port);
            }
      }
      
}

