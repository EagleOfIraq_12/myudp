package team.morabaa.com.myudp.interfaces;

/**
 * Created by eagle on 3/28/2018.
 */
// send data from adapter to fragment

public interface AdapterFragmentBridge {
      void onDeviceClicked(int position);
      
      void onDeviceLongClicked(int position);
}
