package team.morabaa.com.myudp.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;


/**
 * Created by eagle on 11/2/2017.
 */
@Entity
public class Room {
      
      
      @PrimaryKey
      private long id;
      private long controllerId;
      private String name;
      private String floorId;
      private String iconName;
//    private List<Device> children;
      
      public Room() {
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public long getControllerId() {
            return controllerId;
      }
      
      public void setControllerId(long controllerId) {
            this.controllerId = controllerId;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public String getFloorId() {
            return floorId;
      }
      
      public void setFloorId(String floorId) {
            this.floorId = floorId;
      }
      
      public String getIconName() {
            return iconName;
      }
      
      public void setIconName(String iconName) {
            this.iconName = iconName;
      }
      
      @Dao
      public interface RoomDuo {
            
            @Query("SELECT * FROM Room")
            List<Room> ROOMS();
            
            @Query("SELECT name FROM Room WHERE id = :id")
            String getRoomNameByID(long id);
            
            @Query("SELECT name FROM Room")
            List<String> getRoomsNames();
            
            @Query("SELECT id FROM Room WHERE name = :name")
            long getRoomIDByName(String name);
            
            @Query("DELETE FROM Room")
            void deleteAll();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(Room... rooms);
            
            
            @Delete
            void delete(Room... rooms);
            
            @Update
            void Update(Room... rooms);
            
            @Query("SELECT COUNT(id) FROM Room")
            int count();
      }
}
