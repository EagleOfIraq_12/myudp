package team.morabaa.com.myudp.interfaces;

import team.morabaa.com.myudp.model.Device;

/**
 * Created by eagle on 3/28/2018.
 */
// send data from activity to fragment
public interface ActivityFragmentBridge {
      void onDeviceReceived(Device device);
      
}
