package team.morabaa.com.myudp.utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import team.morabaa.com.myudp.model.Device;
import team.morabaa.com.myudp.model.Device.DeviceDuo;
import team.morabaa.com.myudp.model.Floor;
import team.morabaa.com.myudp.model.Floor.FloorDuo;
import team.morabaa.com.myudp.model.Room;
import team.morabaa.com.myudp.model.Room.RoomDuo;

/**
 * Created by eagle on 2/6/2018.
 */
@Database(entities = {
      Device.class,
      Room.class,
      Floor.class,
     }, version = 4)
public abstract class DB extends RoomDatabase {
      
      public abstract DeviceDuo deviceDuo();
      public abstract RoomDuo roomDuo();
      public abstract FloorDuo floorDuo();
      
}
