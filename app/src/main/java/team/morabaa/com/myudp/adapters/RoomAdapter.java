package team.morabaa.com.myudp.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import team.morabaa.com.myudp.R;
import team.morabaa.com.myudp.fragments.RoomFragment;
import team.morabaa.com.myudp.interfaces.AdapterFragmentBridge;
import team.morabaa.com.myudp.model.Device;

/**
 * -Created by eagle on 10/21/2017.
 */


public class RoomAdapter extends RecyclerView.Adapter<RoomViewHolder> {
      
      private AdapterFragmentBridge adapterFragmentBridge;
      @SuppressLint("StaticFieldLeak")
      private int viewId = 0;
      private List<Device> devices;
      private Context ctx;
      private ColorMatrixColorFilter whiteFilter;
      private ColorMatrixColorFilter blueFilter;
      private DisplayMetrics displayMetrics;
      private List<RoomViewHolder> roomViewHolders = new ArrayList<>();
      
      @SuppressLint("HandlerLeak")
      public RoomAdapter(Context ctx, List<Device> devices, long roomId, Activity mainActivity,
            DisplayMetrics displayMetrics, RoomFragment roomFragment) {
            this.devices = devices;
            this.ctx = ctx;
            long roomId1 = roomId;
            this.displayMetrics = displayMetrics;
            adapterFragmentBridge = roomFragment;
      }
      
      @Override
      public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.thing, parent, false);
            
            float[] whiteMatrix = {1.0f, 1.0f, 1.0f, 0, 10, 1.0f, 1.0f, 1.0f, 0, 10, 1.0f, 1.0f,
                  1.0f, 0, 10, 0, 0, 0, 1, 0};
            float[] blueMatrix = {
                  0.0f, 0.0f, 0.0f, 0.0f, 10,
                  0.0f, 0.0f, 0.0f, 0.0f, 10,
                  1.0f, 1.0f, 1.0f, 0.0f, 10,
                  0.0f, 0.0f, 0.0f, 1, 0.0f
            };
            whiteFilter = new ColorMatrixColorFilter(whiteMatrix);
            blueFilter = new ColorMatrixColorFilter(blueMatrix);
            
            return new RoomViewHolder(view);
      }
      
      private void fixSize(View view) {
            if (view != null) {
                  int dim = (int) (displayMetrics.widthPixels / 2.12);
                  view.getLayoutParams().width = dim;
                  view.getLayoutParams().height = dim;
                  view.requestLayout();
            }
      }
      
      @Override
      public void onBindViewHolder(final RoomViewHolder holder,
            @SuppressLint("RecyclerView") int position) {
            holder.deviceId = devices.get(holder.getAdapterPosition()).getDeviceId();
            roomViewHolders.add(holder);
            fixSize(holder.view);
            holder.textView.setText(devices.get(position).getName());
            try {
                  holder.imageView.setImageDrawable(
                        ctx.getResources().getDrawable(
                              ctx.getResources()
                                    .getIdentifier(devices.get(position).getIconName(), "drawable",
                                          ctx.getPackageName())
                        )
                  );
            } catch (Exception e) {
                  holder.imageView.setImageDrawable(
                        ctx.getResources().getDrawable(R.drawable.device_e)
                  );
            }
            
            if (devices.get(position).getStatus() > 0) {
                  holder.imageView.setColorFilter(whiteFilter);
                  holder.out.setBackgroundResource(R.drawable.bg);
                  holder.textView.setTextColor(Color.WHITE);
            } else {
                  holder.imageView.setColorFilter(null);
                  holder.out.setBackground(null);
                  holder.textView.setTextColor(0xff340079);
            }
            
            holder.view.setOnLongClickListener(new View.OnLongClickListener() {
                  @Override
                  public boolean onLongClick(View view) {
                        adapterFragmentBridge.onDeviceLongClicked(position);
                        return false;
                  }
            });
            holder.view.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        adapterFragmentBridge.onDeviceClicked(position);
                  }
            });
      }
      
      @Override
      public int getItemCount() {
            return devices.size();
      }
      
      public List<Device> getDevices() {
            return devices;
      }
      
      public void setDevices(List<Device> devices) {
            this.devices = devices;
            notifyDataSetChanged();
      }
      
      public void clear() {
            this.devices = new ArrayList<>();
            notifyDataSetChanged();
      }
      
      public void updateDevice(Device device) {
            for (RoomViewHolder roomViewHolder : roomViewHolders) {
                  if (roomViewHolder.deviceId == device.getDeviceId()) {
                        for (Device d : devices) {
                              if (d.getDeviceId() == device.getDeviceId()) {
                                    d.setStatus(device.getStatus());
                              }
                        }
                        notifyItemChanged(roomViewHolder.getAdapterPosition());
                  }
            }
      }
      
      // Container must implement this interface
      public void setAdapterFragmentBridge(AdapterFragmentBridge adapterFragmentBridge) {
            this.adapterFragmentBridge = adapterFragmentBridge;
      }
}

class RoomViewHolder extends RecyclerView.ViewHolder {
      
      public long deviceId;
      CardView view;
      TextView textView;
      ImageView imageView;
      LinearLayout out;
      
      RoomViewHolder(View itemView) {
            super(itemView);
            view = (CardView) itemView;
            out = itemView.findViewById(R.id.out);
            imageView = itemView.findViewById(R.id.imgIcon);
            textView = itemView.findViewById(R.id.txtName);
      }
}