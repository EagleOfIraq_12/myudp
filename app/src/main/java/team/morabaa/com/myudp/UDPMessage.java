package team.morabaa.com.myudp;

/**
 * Created by eagle on 12/30/2017.
 */

public class UDPMessage {
    private String message;
    private String body;
    private long arrival;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getArrival() {
        return arrival;
    }

    public void setArrival(long arrival) {
        this.arrival = arrival;
    }
}
