package team.morabaa.com.myudp.tcp;

import static team.morabaa.com.myudp.activities.MainActivity.ctx;
import static team.morabaa.com.myudp.activities.MainActivity.indoor;
import static team.morabaa.com.myudp.utils.SV.SERVER_IP;

import android.content.Context;
import java.io.DataOutputStream;
import java.net.Socket;
import team.morabaa.com.myudp.model.FirebaseMessage;
import team.morabaa.com.myudp.utils.ConnectionState;
import team.morabaa.com.myudp.utils.SV.TCP_COMMANDS;

/**
 * Created by eagle on 2/11/2018.
 */

public class TcpClient {

//      public static String SERVER_IP = "192.168.0.109";
      
      private static String TEMP_SERVER_IP = "";
      
      public TcpClient() {
      }
      
      
      public static void send(Context ctx, String message) {
            SERVER_IP="192.168.0.110";
            new Thread(new Runnable() {
                  String receivedMessage = "";
                  @Override
                  public void run() {
                        System.out.println("SERVER_IP: " + SERVER_IP);
                        if (SERVER_IP.equals("") && TEMP_SERVER_IP.equals("")) {
                              checkServer();
                        }
                        Socket clientSocket = null;
                        try {
                              if (SERVER_IP.equals("")) {
                                    clientSocket = new Socket(TEMP_SERVER_IP, 21111);
                                    
                              } else {
                                    clientSocket = new Socket(SERVER_IP, 21111);
                              }
                              DataOutputStream outToServer = new DataOutputStream(
                                    clientSocket.getOutputStream());
                              
                              outToServer.write(message.getBytes());
                             
                              System.out.println(
                                    "sent To TCP SERVER: " + TEMP_SERVER_IP+ " "+ SERVER_IP + " " + message);
      
                              byte[] buffer = new byte[1400];
                              
                              int r = clientSocket.getInputStream().read(buffer);
                              
                              receivedMessage = new String(buffer).substring(0, r);
                              if (receivedMessage.equals("yes")) {
                                    System.out.println("SERVER_IP: " + SERVER_IP);
                                    SERVER_IP = clientSocket.getInetAddress().getHostAddress();
                              }
                              if (receivedMessage.length() > 0) {
                                    System.out.println("FROM SERVER: " + receivedMessage);
                                    indoor(true);
                              }
                              
                              clientSocket.close();
                        } catch (Exception e) {
                              if (!message.equals(TCP_COMMANDS.CHECK_SERVER)) {
                                    FirebaseMessage.send(ctx, message);
                                    indoor(false);
                              }
                        }
                  }
            }).start();
      }
      
      private static void checkServer() {
            String myIp = ConnectionState.getLocalIpAddress();
            int i = 0;
            while (i++ < 224 && SERVER_IP.equals("")) {
                  TEMP_SERVER_IP = myIp.substring(0, 10) + i;
                  send(ctx, TCP_COMMANDS.CHECK_SERVER);
                  try {
                        Thread.sleep(10);
                  } catch (InterruptedException e) {
                        e.printStackTrace();
                  }
            }
      }
}